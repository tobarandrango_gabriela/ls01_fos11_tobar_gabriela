
public class AusgabenBeispiele {

	public static void main(String[] args) {
		
		//Kommazahl
		System.out.printf(" Zahl: *%-20.2f* %n", 12345.6789);
		
		// Ganzzahl
		System.out.printf(" Zahl: *%-20d* %n", 123456789);
		
		//Zeichenkette (String)
		System.out.printf(" Wort: *%-20.4s* "," abcdef");

	}

}
