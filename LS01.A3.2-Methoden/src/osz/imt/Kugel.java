package osz.imt;

import java.util.Scanner;

public class Kugel {

              private static Scanner scan;

			public static void main(String[] args) {
                            scan = new Scanner(System.in);
                            System.out.println("Geben Sie den Radius des Kugels:");
                            double a = scan.nextDouble();
                            double zahl = 4/3;
                            final double PI = Math.PI;

                            double erg = berechneKugelvolumen(a,PI,zahl);

                            System.out.println("Volum von Kugel:" + erg);

              }

              public static double berechneKugelvolumen(double radius, double zahl, double PI){
                            double ergebnis = zahl / (radius * radius * radius) * PI;
                            return ergebnis;
              }
}
